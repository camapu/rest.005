package net.canos.spring.webapp;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;



public class PersonForm {
	public enum Gender {MALE, FEMALE};
	
	private Integer id;
	@NotNull
	@Size(min=2, max=30)
	private String name;
	@NotNull
	private String surName;
	private Date birthday;
	private Integer height;
	private Integer weight; 
	private Gender gender; 
	
	public PersonForm() {
		super();
	}
	public String getSurName() {
		return surName;
	}
	public void setSurName(String surName) {
		this.surName = surName;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public Integer getHeight() {
		return height;
	}
	public void setHeight(Integer height) {
		this.height = height;
	}
	public Integer getWeight() {
		return weight;
	}
	public void setWeight(Integer weight) {
		this.weight = weight;
	}
	
	@Override
	public String toString() {
		return "Person [name=" + name + ", surName=" + surName + ", birthday=" + birthday + ", height=" + height
				+ ", weight=" + weight + "]";
	}
	public Gender getGender() {
		return gender;
	}
	public void setGender(Gender gender) {
		this.gender = gender;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
}
